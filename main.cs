﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parser
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button1_Click(null, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text=string.Empty; 
            richTextBox2.Text=string.Empty; 
            
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(richTextBox2.Text);
            MessageBox.Show("Результат сохранен в буфер");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = saveFileDialog1.FileName;
            // сохраняем текст в файл
            System.IO.File.WriteAllText(filename, richTextBox2.Text);
            MessageBox.Show("Файл сохранен");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int kolstr = richTextBox1.Lines.Length;
            //richTextBox2.AppendText("строк:"+kolstr.ToString()+"\n");   
           if ( kolstr< 2) { MessageBox.Show("Мало данных"); return; };
            richTextBox2.Clear();
            int z = 1;
            string str=string.Empty;
            for (int i = 0; i < richTextBox1.Lines.Length ; i++) 
            { 
            
                if (checkBox1.Checked) { str += "'"; };// начальная скобка текста
                if (checkBox2.Checked) { str += textBox2.Text; };
                str += richTextBox1.Lines[i];
                if (checkBox3.Checked) {  str += textBox3.Text; };
                if (checkBox1.Checked) { str += "'"; };// конечная скобка текста
                str += ",";
                z++;
                if (z > Convert.ToInt32(textBox1.Text))
                {
                    richTextBox2.AppendText(str + "\n");
                    str=string.Empty;
                    z = 1;
                };

            };
            richTextBox2.AppendText(str);
            str = string.Empty;
            str = richTextBox2.Text.Substring(richTextBox2.Text.Length - 1, 1);
            if (str == ",") { richTextBox2.Text = richTextBox2.Text.Substring(0, richTextBox2.Text.Length - 1); };
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true) { textBox2.Visible = true; } else { textBox2.Visible = false; } ;

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true) { textBox3.Visible = true; } else { textBox3.Visible = false; };
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox4.Text = textBox4.Text.Replace(".", ",");
            double  start_sum = Convert.ToDouble (textBox4.Text);
            double itog_sum = start_sum;
            double nds = 0;// ((start_sum / 100)* 12);
            double nsp = 0;// (start_sum / 100)*2;
            start_sum = 0;// start_sum - nds - nsp;
            textBox7.Text= start_sum.ToString(); 
            textBox6.Text=nsp.ToString();
            textBox5.Text=nds.ToString();
            double doba = 1;
            double summa_obsh = start_sum + nsp + nds;
            while (summa_obsh != itog_sum)
            {
                start_sum += doba;
                 nds = ((start_sum / 100) * 12);
                 nsp = (start_sum / 100) * 2;
                nds = Math.Round(nds,5, MidpointRounding.AwayFromZero);
                nsp= Math.Round(nsp, 5, MidpointRounding.AwayFromZero);
                summa_obsh = start_sum + nsp + nds;
                summa_obsh= Math.Round(summa_obsh, 5, MidpointRounding.AwayFromZero);
                if (summa_obsh > itog_sum)
                {
                    summa_obsh -= doba;
                    start_sum -= doba;
                    doba = doba / 10;
                    if (doba<0.00001) { 
                        nds = ((start_sum / 100) * 12);
                        nsp = (start_sum / 100) * 2;
                        break; };

                };
            };
          //  nds = ((start_sum / 100) * 12);
         //   nsp = (start_sum / 100) * 2;
            nds = Math.Round(nds, 5, MidpointRounding.AwayFromZero);
            nsp = Math.Round(nsp, 5, MidpointRounding.AwayFromZero);
            start_sum = Math.Round(start_sum, 5, MidpointRounding.AwayFromZero);
            textBox7.Text = start_sum.ToString();
            textBox6.Text = nsp.ToString();
            textBox5.Text = nds.ToString();
            summa_obsh = start_sum + nsp + nds;
            summa_obsh = Math.Round(summa_obsh, 5, MidpointRounding.AwayFromZero);
            textBox8.Text=summa_obsh.ToString();    
        }
    }
}
